<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class vpas extends Model
{
    protected $table = 'vpas';
    protected $primaryKey = 'vaid';
	protected $hidden = ['deleted_at'];
}
