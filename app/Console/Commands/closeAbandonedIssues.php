<?php

namespace App\Console\Commands;

use App\deliveries;
use Carbon\Carbon;
use Illuminate\Console\Command;

class closeAbandonedIssues extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'close:abandoned';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Closes issues older than one day';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $deliveries = deliveries::where('status','Issue')->get();

        $count = 0;
        foreach($deliveries as $item){
        	if(Carbon::createFromFormat("Y-m-d H:i:s",$item->created_at) > Carbon::now()->subHours(24)){
		        $item->status = "Complete";
		        $item->save();
		        $count++;
	        }
        }

        // deliveries that have been picked and dropped off
        $deliveries = deliveries::where('status','Pending')->where('pickedUpAt' ,'<>', null)->where('droppedOffAt','<>',null)->get();

	    foreach($deliveries as $item){
		    if(Carbon::createFromFormat("Y-m-d H:i:s",$item->created_at) > Carbon::now()->subHours(24)){
			    $item->status = "Complete";
			    $item->save();
			    $count++;
		    }
	    }

	    // deliveries that have been picked and there's no drop off record
	    $deliveries = deliveries::where('status','Pending')->where('pickedUpAt','<>', null)->where('droppedOffAt',null)->get();

	    foreach($deliveries as $item){
		    if(Carbon::createFromFormat("Y-m-d H:i:s",$item->created_at) > Carbon::now()->subHours(24)){
			    $item->droppedOffAt = Carbon::now()->getTimestamp();
			    $item->description = $item->description . "\n\nDrop-off time inserted by system";
			    $item->status = "Complete";
			    $item->save();
			    $count++;
		    }
	    }

	    $deliveries = deliveries::where('status','Pending')->where('pickedUpAt', null)->get();

	    foreach($deliveries as $item){
		    if(Carbon::createFromFormat("Y-m-d H:i:s",$item->created_at) > Carbon::now()->subHours(24)){
			    $item->status = "Cancelled";
			    $item->save();
			    $count++;
		    }
	    }


	    echo "Closed $count issues";
    }
}
