<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class priceupdate extends Model
{
    protected $primaryKey = 'puid';
    protected $table = 'priceupdates';

	public function Delivery() {
		return $this->belongsTo(deliveries::class,'did','did');
    }

	public function Staff() {
		return $this->belongsTo(User::class,'uid','uid');
    }
}
