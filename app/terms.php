<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class terms extends Model
{
    protected $table = 'terms';
    protected $primaryKey = 'tid';

    protected $hidden = ['deleted_at'];
}
