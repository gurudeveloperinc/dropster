<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class customerLocations extends Model
{
	protected $primaryKey = 'clid';
	protected $table = 'customerlocations';
}
