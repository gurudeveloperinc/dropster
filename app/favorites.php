<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class favorites extends Model
{
    protected $primaryKey = 'cfid';
    protected $table = 'customerfavorites';
}
