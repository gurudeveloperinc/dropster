<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class airtime extends Model
{
    protected $table = 'airtime';
    protected $primaryKey = 'arid';


	public function Dropper() {
		return $this->belongsTo(dropper::class,'drid','drid');
    }

	public function Staff() {
		return $this->belongsTo(User::class,'uid','uid');
    }
}
