<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use function Sodium\version_string;

class vehiclecateogry extends Model
{
    protected $primaryKey = 'vcid';
    protected $table = 'vehicle_categories';

    public function vehicle()
    {
        return $this->hasMany( vehicle::class, 'vcid');
    }

}
