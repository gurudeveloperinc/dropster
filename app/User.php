<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

	use SoftDeletes;
    protected $primaryKey = 'uid';
    protected $dates = ['deleted_at'];


    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//



    protected $guarded = [ ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
