<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class update extends Model
{
    protected $primaryKey = 'upid';

    protected $table = 'updates';

	public function Delivery(  ) {
		return $this->belongsTo(deliveries::class,'did','did');
    }

	public function Staff() {
		return $this->belongsTo(User::class,'uid','uid');
    }
}
