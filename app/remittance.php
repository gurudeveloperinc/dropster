<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class remittance extends Model
{
    protected $primaryKey = 'rmid';

	public function User() {
		return $this->belongsTo(User::class,'uid','uid');
    }
}
