<?php

namespace App\Mail;

use App\setting;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class paymentReceipt extends Mailable
{
    use Queueable, SerializesModels;

    public $delivery;
    public $baseFare;
    public $costPerKm;

    public function __construct($delivery)
    {
        $this->delivery = $delivery;
	    $this->baseFare = setting::where('name','baseFare')->get()->last();
	    $this->costPerKm = setting::where('name','costPerKm')->get()->last();

    }


    public function build()
    {
        return $this->view('emails.paymentReceipt');
    }
}
