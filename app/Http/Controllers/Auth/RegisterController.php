<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/register';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'fname' => 'required|string|max:255',
            'sname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|string',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }




    public function register(Request $request)
    {

	    $filename = $request->file('image')->getClientOriginalName();
	    $request->file('image')->move('profileImages', $filename);

	    $profileUrl = url('/profileImages/' . $filename);

	    $this->validator($request->all())->validate();

	    event(new Registered($user = $this->create($request->all(), $profileUrl)));

	    $request->session()->flash('success','Employee created successfully');

	    return $this->registered($request, $user)
		    ?: redirect($this->redirectPath());
    }




    protected function create(array $data, $profileImage)
    {


        return User::create([

            'sid'  => $data['sid'],
            'role' => $data['role'],
            'fname' => $data['fname'],
            'sname' => $data['sname'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'phone' => $data['phone'],
            'address' => $data['address'],
            'maritalStatus' => $data['maritalStatus'],
            'gender'  => $data['gender'],
            'dob'     => $data['dob'],
            'image'   => $profileImage

        ]);

    }
}
