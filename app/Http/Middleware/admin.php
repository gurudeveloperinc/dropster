<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
	    if(Auth::user()->role == "Admin"){
		    return $next($request);
	    } else{

	    	if($request->path() == 'staff/' . Auth::user()->uid ||
		       $request->path() == 'staff/' . Auth::user()->uid . '/edit') return $next($request);
	    	else return redirect('/');
	    }

    }
}
