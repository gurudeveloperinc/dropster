<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class vehicle_assignment_histories extends Model
{
     protected $primaryKey ='vahid';

	public function Vehicle() {
		return $this->belongsTo(vehicle::class, 'vid','vid');
     }

}
