<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class vppayment extends Model
{
    protected $table = 'vppayments';
    protected $primaryKey = 'vppid';

	public function Vehicle() {
		return $this->belongsTo(vehicle::class,'vid','vid');
    }

}
