<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class data extends Model
{
    protected $table = 'data';
    protected $primaryKey = 'daid';

	public function Dropper() {
		return $this->belongsTo(dropper::class,'drid','drid');
	}

	public function Staff() {
		return $this->belongsTo(User::class,'uid','uid');
	}

}
