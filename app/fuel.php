<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class fuel extends Model
{
    protected $primaryKey = 'fid';
    protected $table = 'fuel';

	public function User() {
		return $this->belongsTo(User::class,'uid','uid');
    }

	public function Vehicle() {
		return $this->belongsTo(vehicle::class,'vid','vid');
    }


}
