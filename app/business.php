<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class business extends Model
{
    //

	use SoftDeletes;
	protected  $primaryKey = 'bid';
	protected  $dates = ['deleted_at'];



	protected $hidden = [
		'password', 'remember_token',
	];
}
