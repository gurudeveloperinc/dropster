<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class coupon extends Model
{
    protected $primaryKey = 'coupid';
    public $table = 'coupons';

	public function Usages() {
		return $this->hasMany(couponUse::class,'coupid','coupid');
    }
}
