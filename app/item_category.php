<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class item_category extends Model
{
    protected $primaryKey ='icid';

    public  $table = 'item_categories';

	public function Items( ) {
		 return $this->hasMany(delivery_item::class, 'icid', 'icid');
}


}
