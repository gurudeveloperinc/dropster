<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAirtimeAndData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('airtime', function(Blueprint $table){
            $table->increments('arid');
            $table->integer('drid');
            $table->integer('uid');
            $table->integer('amount');
            $table->string('network');
            $table->timestamps();
        });

        Schema::create('data', function(Blueprint $table){
	        $table->increments('daid');
	        $table->integer('drid');
	        $table->integer('uid');
	        $table->integer('amount');
	        $table->string('network');
	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('airtime');
        Schema::dropIfExists('data');
    }
}
