<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyDb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deliveries', function(Blueprint $table){
            $table->boolean('isNotified')->default(0);
            $table->boolean('isFromStaff')->default(0);
            $table->boolean('isScheduled')->default(0);
            $table->integer('uid')->nullable();
            $table->timestamp('schedule')->nullable();
            $table->integer('controller')->nullable()->default(null);


            $table->string('xfname')->nullable();
            $table->string('xsname')->nullable();
            $table->string('xphone')->nullable();
            $table->string('xemail')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
