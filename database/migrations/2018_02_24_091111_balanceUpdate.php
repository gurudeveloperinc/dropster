<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BalanceUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customerBalanceUpdate', function(Blueprint $table){
            $table->increments('cbid');
            $table->integer('uid');
            $table->integer('cid');
            $table->integer('amount');
            $table->string('reason',1000);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customerBalanceUpdate');
    }
}
