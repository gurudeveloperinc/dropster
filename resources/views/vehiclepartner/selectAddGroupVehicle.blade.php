@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- .row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">

                            <table class="table table-bordered">
                                <tr>
                                    <th>Group name</th>
                                    <th>Number Of Members</th>
                                    <th></th>
                                </tr>

                                @foreach($groups as $item)
                                    <tr>
                                        <td>{{$item->name}}</td>
                                        <td>{{count($item->Members)}}</td>
                                        <td>
                                            <a href="{{url('/add-group-vehicle/' . $item->gid)}}" class="label label-success">Add Vehicle</a>
                                        </td>
                                    </tr>

                                @endforeach
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection