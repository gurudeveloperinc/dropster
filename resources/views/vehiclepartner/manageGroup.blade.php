@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- .row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">

                            <table class="table table-bordered">
                                <tr>
                                    <th>Group name</th>
                                    <th>Number Of Members</th>
                                    <th>Number Of Vehicles</th>
                                    <th></th>
                                </tr>

                                @foreach($groups as $item)
                                <tr>
                                    <td>{{$item->name}}</td>
                                    <td>{{count($item->Members)}}</td>
                                    <td>{{count($item->Vehicles)}}</td>
                                    <td>
                                        <a href="{{url('/manage-group/' . $item->gid)}}" class="label label-success">View</a>
                                    </td>
                                </tr>

                                @endforeach
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection