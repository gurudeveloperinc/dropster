@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- .row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">

                            @include('notification')
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Current Settings</div>

                                        <div class="row">
                                            <table class="table table-bordered">
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Value</th>
                                                </tr>

                                                <tr>
                                                    <td>Base Fare (₦)</td>
                                                    <td>{{$basefare->value}}</td>
                                                </tr>

                                                <tr>
                                                    <td>Car Cost Per Km (₦)</td>
                                                    <td>{{$carCostPerKm->value}}</td>
                                                </tr>

                                                <tr>
                                                    <td>Bike Cost Per Km (₦)</td>
                                                    <td>{{$costperkm->value}}</td>
                                                </tr>

                                                <tr>
                                                    <td>Referral Bonus (₦)</td>
                                                    <td>{{$referralBonus->value}}</td>
                                                </tr>

                                                <tr>
                                                    <td>Admin Percent (%)</td>
                                                    <td>{{$adminPercent->value}}</td>
                                                </tr>

                                                <tr>
                                                    <td>Maintenance Percent (%)</td>
                                                    <td>{{$maintenancePercent->value}}</td>
                                                </tr>

                                                <tr>
                                                    <td>Partner Percent (%)</td>
                                                    <td>{{$partnerPercent->value}}</td>
                                                </tr>

                                                <tr>
                                                    <td>Cancel Minutes</td>
                                                    <td>{{$cancelMinutes->value}}</td>
                                                </tr>

                                                <tr>
                                                    <td>Cancellation Fee (₦)</td>
                                                    <td>{{$cancellationFee->value}}</td>
                                                </tr>

                                                <tr>
                                                    <td>Maintenance Schedule (In Weeks)</td>
                                                    <td>{{$maintenanceSchedule->value}}</td>
                                                </tr>


                                            </table>

                                        </div>

                                        <hr>
                                        <label for="role" class="col-md-4 control-label"> CHANGE SETTINGS </label>

                                        <div class="panel-body">

                                            <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{ url('/post-settings') }}">
                                                {{ csrf_field() }}




                                                <div class="form-group">
                                                    <label for="role" class="col-md-4 control-label"> Prices </label>

                                                    <div class="col-md-6">
                                                        <select class="form-control" name="name">
                                                            <option value="baseFare">Base fare</option>
                                                            <option value="carCostPerKm">Car Cost Per Km</option>
                                                            <option value="costPerKm">Bike Cost Per Km</option>
                                                            <option value="referralBonus">Refferal Bonus</option>
                                                            <option value="adminPercent">Admin Percent</option>
                                                            <option value="maintenancePercent">Maintenance Percent</option>
                                                            <option value="partnerPercent">Vehicle Partner Percent</option>
                                                            <option value="cancelMinutes">Cancel Minutes</option>
                                                            <option value="cancellationFee">Cancellation Fee</option>
                                                            <option value="maintenanceSchedule">Maintenance Schedule</option>

                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label for="value" class="col-md-4 control-label">Value(₦)</label>

                                                    <div class="col-md-6">
                                                        <input id="value" type="text" class="form-control" name="value" value="{{ old('value') }}" required autofocus>
                                                    </div>
                                                </div>



                                                <div class="form-group">
                                                    <div class="col-md-10 col-md-offset-4">
                                                        <button type="submit" class="btn btn-primary pull-right">
                                                            CHANGE
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>

@endsection


