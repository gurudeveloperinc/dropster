@extends('layouts.app')

@section('content')

<div class="container">
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Add a New Vehicle Category</div>
            <div class="panel-body">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/add-vehicle-cat') }}">
                    {{ csrf_field() }}



                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label"> Category Name</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                            <label for="email" class="col-md-4 control-label"> Description</label>

                                            <div class="col-md-6">
                                                <input id="description" type="text" class="form-control" name="description" value="{{ old('description') }}" required autofocus>

                                                @if ($errors->has('description'))
                                                    <span class="help-block">
                                                            <strong>{{ $errors->first('description') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                    </div>
                    <div class="form-group{{ $errors->has('maxHeight') ? ' has-error' : '' }}">
                                            <label for="maxHeight" class="col-md-4 control-label"> Max Height</label>

                                            <div class="col-md-6">
                                                <input id="maxHeight" type="text" class="form-control" name="maxHeight" value="{{ old('maxHeight') }}" required autofocus>

                                                @if ($errors->has('maxHeight'))
                                                    <span class="help-block">
                                                            <strong>{{ $errors->first('maxHeight') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                            </div>

                                        <div class="form-group{{ $errors->has('maxWidth') ? ' has-error' : '' }}">
                                                    <label for="maxWidth" class="col-md-4 control-label"> Max width</label>

                                                    <div class="col-md-6">
                                                        <input id="maxWidth" type="text" class="form-control" name="maxWidth" value="{{ old('maxWidth') }}" required autofocus>

                                                        @if ($errors->has('maxWidth'))
                                                            <span class="help-block">
                                                                    <strong>{{ $errors->first('maxWidth') }}</strong>
                                                                </span>
                                                        @endif
                                                    </div>
                                              </div>
                                                 <div class="form-group{{ $errors->has('maxDepth') ? ' has-error' : '' }}">
                                                    <label for="maxDepth" class="col-md-4 control-label"> Max Depth</label>

                                                    <div class="col-md-6">
                                                        <input id="maxDepth" type="text" class="form-control" name="maxDepth" value="{{ old('maxDepth') }}" required autofocus>

                                                        @if ($errors->has('maxDepth'))
                                                            <span class="help-block">
                                                                    <strong>{{ $errors->first('maxDepth') }}</strong>
                                                                </span>
                                                        @endif
                                                    </div>
                                                       </div>

                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Add Category
                            </button>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@endsection