@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- .row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">

                            <iframe src="https://dashboard.tawk.to" width="200" style="width:100%; height: 100%; min-height: 500px;"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection