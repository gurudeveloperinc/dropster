

<?php use Carbon\Carbon; ?>
@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- .row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">

                        @include('notification')

                        <!-- .row -->
                            <div class="row bg-title" >
                                <div class="col-lg-12">
                                </div>
                            </div>

                            <!-- .row -->
                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="white-box">
                                        <div class="user-bg"> <img width="100%" alt="user" src="{{url('img/landscape17.jpg')}}">
                                            <div class="overlay-box">
                                                <div class="user-content">
                                                    <a href="javascript:void(0)"><img src="{{$staff->image}}" class="thumb-lg img-circle" alt="img"></a>
                                                    <h4 class="text-white">{{$staff->fname}} {{$staff->sname}}</h4>
                                                    <h5 class="text-white">Created {{Carbon::createFromFormat("Y-m-d H:i:s",$staff->created_at)->toFormattedDateString()}} </h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="user-btm-box">
                                            <div class="col-md-4 col-sm-4 text-center">

                                            </div>
                                            <div class="col-md-4 col-sm-4 text-center">

                                            </div>
                                            <div class="col-md-4 col-sm-4 text-center">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-12">
                                    <div class="white-box">
                                        <ul class="nav customtab nav-tabs" role="tablist">

                                            <li role="presentation" class="nav-item"><a href="#basic" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Basic</span></a></li>
                                            <li role="presentation" class="nav-item"><a href="#details" class="nav-link" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Details</span></a></li>
                                        </ul>
                                        <div class="tab-content">

                                            <!-- start tab 1 -->
                                            <div class="tab-pane active" id="basic">
                                                <div class="row">
                                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Gender</strong>
                                                        <br>
                                                        <p class="text-muted">{{ $staff->gender }}</p>
                                                    </div>
                                                    <div class="col-md-3 col-xs-6 b-r">
                                                        <strong>Role</strong>
                                                        <br>
                                                        <p class="text-muted">{{ $staff->role }}</p>
                                                    </div>
                                                    <div class="col-md-3 col-xs-6 b-r">
                                                        <strong>Joined</strong>
                                                        <br>
                                                        <p class="text-muted">{{Carbon::createFromFormat("Y-m-d H:i:s",$staff->created_at)->diffForHumans()}}</p>
                                                    </div>

                                                </div>


                                                <hr>

                                                <table class="table table-bordered">
                                                    <tr>
                                                        <th>First Name</th>
                                                        <th>Surname</th>
                                                        <th>Phone</th>
                                                        <th>Email</th>
                                                    </tr>


                                                        <tr>
                                                            <td>{{$staff->fname}}</td>
                                                            <td>{{$staff->sname}}</td>
                                                            <td>{{$staff->phone}}</td>
                                                            <td>{{$staff->email}}</td>
                                                        </tr>

                                                </table>

                                                <a href="{{url('/staff/'. $staff->uid . '/edit')}}" class="btn btn-success">Edit</a>
                                                <a href="{{url('/staff/')}}" class="btn btn-warning">Back</a>

                                            </div>


                                            <!-- start tab 2  -->
                                            <div class="tab-pane" id="details">

                                                <div class="form-group">
                                                    <label class="col-md-12">Firstname</label>
                                                    <div class="col-md-12">
                                                        <p>  {{$staff->fname}}</p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-12">Surname</label>
                                                    <div class="col-md-12">
                                                        <p>  {{$staff->sname}}</p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-12">Email</label>
                                                    <div class="col-md-12">
                                                        <p>  {{$staff->email}}</p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-12">Phone</label>
                                                    <div class="col-md-12">
                                                        <p>  {{$staff->phone}}</p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-12">Marital Status</label>
                                                    <div class="col-md-12">
                                                        <p>  {{$staff->maritalStatus}}</p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-12">Gender</label>
                                                    <div class="col-md-12">
                                                        <p>  {{$staff->gender}}</p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-12">DOB</label>
                                                    <div class="col-md-12">
                                                        <p>  {{$staff->dob}}</p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-12">Address</label>
                                                    <div class="col-md-12">
                                                        <p>  {{$staff->address}}</p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <a href="{{url('/staff/'. $staff->uid . '/edit')}}" class="btn btn-success">Edit</a>
                                                        <a href="{{url('/staff/')}}" class="btn btn-warning">Back</a>

                                                    </div>
                                                </div>

                                            </div>

                                        <!-- end tab 2 -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection