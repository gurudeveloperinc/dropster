

@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- .row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">
                            @include('notification')
                            <h3 class="box-title m-b-0">Registered Staff</h3>
                            <table id="demo-foo-addrow" class="table table-bordered table-hover toggle-circle" data-page-size="7">
                                <thead>
                                <tr>
                                    <th data-sort-initial="true" data-toggle="true">First Name</th>
                                    <th>Last Name</th>
                                    <th>Phone</th>
                                    <th data-hide="phone, tablet">Email</th>
                                    <th data-hide="phone, tablet">Role</th>
                                    <th data-sort-ignore="true" class="min-width"> </th>
                                </tr>
                                </thead>
                                <div class="form-inline padding-bottom-15">
                                    <div class="row">

                                    </div>
                                    <div class="col-sm-12 text-right m-b-20 pull-right">
                                        <div class="form-group">
                                            <input id="demo-input-search2" type="text" placeholder="Search" class="form-control" autocomplete="on" name="term">
                                        </div>
                                    </div>
                                </div>

                                <tbody>


                                @foreach($staffs as $staff)
                                    <tr>
                                        <td>{{$staff->fname}}</td>
                                        <td>{{$staff->sname}}</td>
                                        <td>{{$staff->phone}}</td>
                                        <td>{{$staff->email}}</td>
                                        <td>{{$staff->role}}</td>
                                        <td>
                                            <a href="{{url('/staff/' . $staff->uid)}}">
                                                <span class="label label-success">View Profile</span>
                                            </a>

                                            <a href="{{url('staff/' . $staff->uid . '/edit')}}">
                                                <span class="label label-warning">Edit</span>
                                            </a>

                                            <a href="{{url('staff/delete/' . $staff->uid)}}">
                                                <span class="label label-danger">Delete</span>
                                            </a>

                                            {{--<button type="button" class="btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i></button>--}}
                                        </td>
                                    </tr>

                                @endforeach


                                </tbody>

                                <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <div class="text-right">
                                            <ul class="pagination">
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection