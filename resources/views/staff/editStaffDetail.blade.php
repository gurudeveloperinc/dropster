

<?php use Carbon\Carbon; ?>
@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- .row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">

                        @include('notification')

                        <!-- .row -->
                            <div class="row bg-title" >
                                <div class="col-lg-12">
                                </div>
                            </div>

                            <!-- .row -->
                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="white-box">
                                        <div class="user-bg"> <img width="100%" alt="user" src="{{url('img/landscape17.jpg')}}">
                                            <div class="overlay-box">
                                                <div class="user-content">
                                                    <a href="javascript:void(0)"><img src="{{$staff->image}}" class="thumb-lg img-circle" alt="img"></a>
                                                    <h4 class="text-white">{{$staff->fname}} {{$staff->sname}}</h4>
                                                    <h5 class="text-white">Created {{Carbon::createFromFormat("Y-m-d H:i:s",$staff->created_at)->toFormattedDateString()}} </h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="user-btm-box">
                                            <div class="col-md-4 col-sm-4 text-center">

                                            </div>
                                            <div class="col-md-4 col-sm-4 text-center">

                                            </div>
                                            <div class="col-md-4 col-sm-4 text-center">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-12">
                                    <div class="white-box">
                                        <ul class="nav customtab nav-tabs" role="tablist">

                                            <li role="presentation" class="nav-item"><a href="#basic" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Basic</span></a></li>
                                            <li role="presentation" class="nav-item"><a href="#details" class="nav-link" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Details</span></a></li>
                                        </ul>
                                        <div class="tab-content">

                                            <!-- start tab 1 -->
                                            <div class="tab-pane" id="basic">
                                                <div class="row">
                                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Gender</strong>
                                                        <br>
                                                        <p class="text-muted">{{ $staff->gender }}</p>
                                                    </div>
                                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Role</strong>
                                                        <br>
                                                        <p class="text-muted">{{ $staff->role }}</p>
                                                    </div>
                                                    <div class="col-md-3 col-xs-6 b-r">

                                                    </div>

                                                </div>


                                                <hr>

                                                <table class="table table-bordered">
                                                    <tr>
                                                        <th>First Name</th>
                                                        <th>Surname</th>
                                                        <th>Phone</th>
                                                        <th>Email</th>
                                                    </tr>


                                                    <tr>
                                                        <td>{{$staff->fname}}</td>
                                                        <td>{{$staff->sname}}</td>
                                                        <td>{{$staff->phone}}</td>
                                                        <td>{{$staff->email}}</td>
                                                    </tr>

                                                </table>

                                                <a href="{{url('/staff/'. $staff->uid . '/edit')}}" class="btn btn-success">Edit</a>
                                                <a href="{{url('/staff/')}}" class="btn btn-warning">Back</a>

                                            </div>


                                            <!-- start tab 2  -->
                                            <div class="tab-pane active" id="details">

                                                <form class="form-horizontal form-material"  method="POST" enctype="multipart/form-data" action="{{ url('/staff/' . $staff->uid . '/edit') }}">
                                                    {{ csrf_field() }}

                                                    <div class="form-group{{ $errors->has('fname') ? ' has-error' : '' }}">
                                                        <label for="name" class="col-md-4 control-label">First Name</label>

                                                        <div class="col-md-6">
                                                            <input id="name" type="text" class="form-control" name="fname" value="{{ $staff->fname }}" required autofocus>

                                                            @if ($errors->has('fname'))
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('fname') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>


                                                    </div>

                                                    <div class="form-group{{ $errors->has('sname') ? ' has-error' : '' }}">
                                                        <label for="name" class="col-md-4 control-label">Last Name</label>

                                                        <div class="col-md-6">
                                                            <input id="name" type="text" class="form-control" name="sname" value="{{ $staff->sname }}" required autofocus>

                                                            @if ($errors->has('sname'))
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('sname') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-group{{ $errors->has('dob') ? ' has-error' : '' }}">
                                                        <label for="dob" class="col-md-4 control-label">Date of Birth</label>

                                                        <div class="col-md-6">
                                                            <input id="dob" type="text" class="form-control" name="dob" value="{{ $staff->dob }}" required autofocus>

                                                            @if ($errors->has('dob'))
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('dob') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>



                                                    <div class="form-group{{ $errors->has('sid') ? ' has-error' : '' }}">
                                                        <label for="sid" class="col-md-4 control-label">Staff ID</label>

                                                        <div class="col-md-6">
                                                            <input id="sid" type="text" class="form-control" name="sid" value="{{ $staff->sid }}" required autofocus>

                                                            @if ($errors->has('sid'))
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('sid') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>


                                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                        <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                                        <div class="col-md-6">
                                                            <input id="email" type="email" class="form-control" name="email" value="{{ $staff->email }}" required>

                                                            @if ($errors->has('email'))
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('email') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>



                                                    <div class="form-group">
                                                        <label for="role" class="col-md-4 control-label">Role</label>

                                                        <div class="col-md-6">
                                                            <select class="form-control" name="role" value="{{$staff->role}}">
                                                                <option>Admin</option>
                                                                <option>Staff</option>

                                                            </select>
                                                        </div>
                                                    </div>


                                                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                                        <label for="phone" class="col-md-4 control-label">Phone Number</label>

                                                        <div class="col-md-6">
                                                            <input id="phone" type="text" pattern="[0-9]+" class="form-control" name="phone" value="{{ $staff->phone}}" required autofocus>

                                                            @if ($errors->has('phone'))
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('phone') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>




                                                    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                                        <label for="address" class="col-md-4 control-label">Address</label>

                                                        <div class="col-md-6">
                                                            <textarea class="form-control" id="address" name="address" required>{{$staff->address }}</textarea>

                                                            @if ($errors->has('address'))
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('address') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="maritalStatus" class="col-md-4 control-label">Marital Status</label>

                                                        <div class="col-md-6">
                                                            <select class="form-control" name="maritalStatus">

                                                                <option
                                                                @if($staff->maritalStatus == "Single")
                                                                    checked
                                                                @endif
                                                                >Single</option>
                                                                <option
                                                                @if($staff->maritalStatus == "Married")
                                                                checked
                                                                @endif
                                                                >Married</option>

                                                            </select>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <label for="gender" class="col-md-4 control-label">Gender</label>

                                                        <div class="col-md-6">
                                                            <select class="form-control" name="gender" value="{{$staff->gender }}">
                                                                <option>Male</option>
                                                                <option>Female</option>
                                                                <option>Others</option>

                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                                        <label for="image" class="col-md-4 control-label">Upload Profile Image</label>

                                                        <div class="col-md-6">
                                                            <input id="image" type="file" class="form-control" name="image" value="{{$staff->image }}">

                                                            @if ($errors->has('image'))
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('image') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>





                                                    <div class="form-group">
                                                        <div class="col-md-6 col-md-offset-4">
                                                            <button class="btn btn-primary">
                                                                Update
                                                            </button>
                                                            <a href="{{url('/staff/'. $staff->uid )}}" class="btn btn-warning">Cancel</a>

                                                        </div>
                                                    </div>
                                                </form>
                                            <!-- end tab 2 -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
