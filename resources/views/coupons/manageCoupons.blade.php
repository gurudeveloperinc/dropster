<?php use Carbon\Carbon; ?>
@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- .row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Manage Coupons</h3>
                            <table id="demo-foo-addrow" class="table table-bordered table-hover toggle-circle" data-page-size="7">
                                <thead>
                                <tr>
                                    <th data-sort-initial="true" data-toggle="true">Coupon Code</th>
                                    <th  data-toggle="true">Value</th>
                                    <th  >Status</th>
                                    <th  data-toggle="true">Max Usages</th>
                                    <th  data-toggle="true">Current Usage</th>
                                    <th data-toggle="true">Expiry Date</th>
                                    <th data-toggle="true">Date Created</th>
                                </tr>
                                </thead>
                                <div class="form-inline padding-bottom-15">
                                    <div class="row">

                                    </div>
                                    <div class="col-sm-6 text-right m-b-20">
                                        <div class="form-group">
                                            <input id="demo-input-search2" type="text" placeholder="Search" class="form-control" autocomplete="off">
                                        </div>
                                    </div>
                                </div>

                                <tbody>

                                @foreach($coupons as $item)
                                    <tr>
                                        <td>{{$item->code}}</td>
                                        <td>{{$item->value}}</td>
                                        <td>{{$item->status}}</td>
                                        <td>{{$item->maxUsages}}</td>
                                        <td>{{ count($item->Usages) }}</td>
                                        <td>{{Carbon::createFromFormat("Y-m-d H:i:s",$item->expiryDate)->toDayDateTimeString()}}</td>
                                        <td>{{Carbon::createFromFormat("Y-m-d H:i:s",$item->created_at)->diffForHumans()}}</td>
                                    </tr>

                                @endforeach

                                </tbody>

                                <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <div class="text-right">
                                            <ul class="pagination">
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection