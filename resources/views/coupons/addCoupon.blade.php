@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div id="page-wrapper">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-lg-12">
                        @include('notification')
                        <div class="white-box">

                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <div class="panel panel-default">

                                        <div class="panel-heading">Add Coupon</div>

                                        <div class="panel-body">



                                                <form class="form-horizontal" role="form" method="POST" action="{{ url('/add-coupon') }}">
                                                    {{ csrf_field() }}

                                                    <div class="form-group">
                                                        <label for="value" class="col-md-4 control-label"> Coupon Code </label>

                                                        <div class="col-md-6">
                                                            <input id="code" type="text" class="form-control" name="code" value="{{ old('code')  }}" required autofocus>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <label for="value" class="col-md-4 control-label"> Value (NGN) </label>


                                                        <div class="col-md-6">
                                                            <input id="value" type="number" class="form-control" name="value" value="{{ old('value')  }}" required autofocus>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <label for="days" class="col-md-4 control-label"> Expiry (In days from now) </label>


                                                        <div class="col-md-6">
                                                            <input id="days" type="number" class="form-control" name="days" value="{{ old('days')  }}" required autofocus>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <label for="maxUsages" class="col-md-4 control-label">Max Usages </label>


                                                        <div class="col-md-6">
                                                            <input id="maxUsages" type="number" class="form-control" name="maxUsages" value="{{ old('maxUsages')  }}" required autofocus>
                                                        </div>
                                                    </div>


                                                    <div class="form-group" >
                                                        <div class="col-md-10 col-md-offset-4">
                                                            {{--<a href="{{url('/ma')}}" style="margin-left:10px;" class="btn btn-warning pull-right">Back</a>--}}
                                                            <button type="submit" class="btn btn-primary pull-right">
                                                                Add
                                                            </button>
                                                        </div>

                                                    </div>
                                                </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>

@endsection


