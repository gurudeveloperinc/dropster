<?php use Carbon\Carbon; ?>
@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- .row -->
                <div class="row">
                    <div class="col-lg-12">
                        @include('notification')
                        <div class="white-box">
                            <h3 class="box-title m-b-0">FAQs</h3>
                            <a href="{{url('add-faq')}}" style="float:right;">
                                <span class="btn btn-success">Add FAQ</span>
                            </a>

                            <table id="demo-foo-addrow" class="table table-bordered table-hover toggle-circle" data-page-size="10">
                                <thead>
                                <tr>
                                    <th data-sort-initial="true" data-toggle="true">Question</th>
                                    <th data-toggle="true">Answer</th>
                                    <th data-toggle="true">Date Created</th>
                                    <th data-toggle="true">Updated</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <div class="form-inline padding-bottom-15">
                                    <div class="row">

                                    </div>
                                    <div class="col-sm-12 text-right m-b-20">
                                        <div class="form-group">
                                            <input id="demo-input-search2" type="text" placeholder="Search" class="form-control" autocomplete="off">
                                        </div>
                                    </div>
                                </div>

                                <tbody>

                                @foreach($faqs as $faq)
                                    <tr>
                                        <td>{{$faq->question}}</td>
                                        <td>{{$faq->answer}}</td>
                                        <td>{{Carbon::createFromFormat("Y-m-d H:i:s",$faq->created_at)->toDayDateTimeString()}}</td>
                                        <td>{{Carbon::createFromFormat("Y-m-d H:i:s",$faq->updated_at)->diffForHumans()}}</td>

                                        <td style="width: 150px;">
                                            <a href="{{url('edit-faq/' . $faq->fid)}}">
                                                <span class="label label-success">Edit</span>
                                            </a>
                                            <a href="{{url('delete-faq/' . $faq->fid)}}">
                                                <span class="label label-danger">Delete</span>
                                            </a>
                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>

                                <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <div class="text-right">
                                            <ul class="pagination">
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection