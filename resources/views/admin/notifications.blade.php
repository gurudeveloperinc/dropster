<?php use Carbon\Carbon; ?>
@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- .row -->
                <div class="row">
                    <div class="col-lg-12">
                        @include('notification')
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Notifications</h3>
                            <a href="{{url('send-notification')}}" style="float:right;">
                                <span class="btn btn-success">Send Push Notification</span>
                            </a>

                            <table class="table table-hover toggle-circle" data-page-size="10">
                                <thead>
                                <tr>
                                    <th data-toggle="true">Message</th>
                                    <th data-toggle="true">Date Sent</th>
                                    <th data-toggle="true">Sent By</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <div class="form-inline padding-bottom-15">
                                    <div class="row">

                                    </div>
                                    <div class="col-sm-12 text-right m-b-20">
                                        <div class="form-group">
                                            <input id="demo-input-search2" type="text" placeholder="Search" class="form-control" autocomplete="off">
                                        </div>
                                    </div>
                                </div>

                                <tbody>

                                @foreach($notifications as $notification)
                                    <tr>
                                        <td>{{$notification->message}}</td>
                                        <td>{{$notification->created_at->toDayDateTimeString()}}</td>
                                        <td>{{$notification->User->fname}} {{$notification->User->sname}}</td>
                                    </tr>

                                @endforeach

                                </tbody>

                                <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <div class="text-right">
                                            <ul class="pagination">
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection