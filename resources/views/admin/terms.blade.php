<?php use Carbon\Carbon; ?>
@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div id="page-wrapper">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-lg-12">
                        @include('notification')
                        <div class="white-box">

                            <div class="row">
                                <div class="col-md-12 ">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Update Terms</div>

                                        <div class="panel-body">

                                            @if(isset($terms))
                                            <span>Last updated: {{Carbon::createFromFormat("Y-m-d H:i:s",$terms->created_at)->toDayDateTimeString()}}</span>
                                            @endif
                                            <form class="form-horizontal" role="form" method="POST" action="{{ url('terms') }}">
                                                {{ csrf_field() }}

                                                <div class="form-group">
                                                    <label for="description" class="control-label">&nbsp;Content</label>

                                                    <div class="col-md-12">
                                                            <textarea id="description"  class="form-control" name="content" required>@if(isset($terms)){{$terms->content}}@endif</textarea>
                                                    </div>
                                                </div>



                                                <div class="form-group" >
                                                    <div class="col-md-12 ">

                                                        <button type="submit" class="btn btn-primary pull-right">
                                                            Update
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>



                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection


