@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div id="page-wrapper">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-lg-12">
                        @include('notification')
                        <div class="white-box">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Add FAQ</div>

                                        <div class="panel-body">

                                                <form class="form-horizontal" role="form" method="POST" action="{{ url('faqs') }}">
                                                    {{ csrf_field() }}


                                                    <div class="form-group">
                                                        <label for="role" class="col-md-4 control-label"> Question </label>


                                                        <div class="col-md-6">
                                                            <input id="name" type="text" class="form-control" name="question" value="{{ old('name') }}" required autofocus>
                                                        </div>
                                                    </div>


                                                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                                        <label for="description" class="col-md-4 control-label">Answer</label>

                                                        <div class="col-md-6">
                                                            <textarea id="description"  class="form-control" name="answer" required></textarea>
                                                        </div>
                                                    </div>



                                                    <div class="form-group" >
                                                        <div class="col-md-10 col-md-offset-4">

                                                            <button type="submit" class="btn btn-primary pull-right">
                                                                Add FAQ
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>



                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>

@endsection


