@extends('layouts.app')

@section('content')


    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js" ></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    <div class="container-fluid" >
        <div id="page-wrapper">
            <div class="container-fluid">


                <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">

                            <div id="app" xmlns:v-bind="http://www.w3.org/1999/xhtml"
                                 xmlns:v-on="http://www.w3.org/1999/xhtml">


                                <label>Table Length: </label>
                                <select v-model="length" v-on:change="search">
                                    <option selected>10</option>
                                    <option>20</option>
                                    <option>50</option>
                                    <option>100</option>
                                    <option>200</option>
                                </select>

                                <label>Filter Requests By: </label>
                                <select v-model="by">
                                    <option selected value="fname">Firstname</option>
                                    <option value="sname">Surname</option>
                                    <option value="email">Email</option>
                                    <option value="phone">Phone</option>
                                    <option value="from">From</option>
                                    <option value="to">To</option>
                                    <option value="status">Status</option>
                                    <option value="paymentStatus">Payment Status</option>
                                </select>

                                <input type="text" v-model="term" v-on:keyup="search">
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination">
                                        <li v-bind:class="[{disabled: !pagination.prev_page_url}]" class="page-item"><a class="page-link" href="#"
                                                                                                                        @click="getItems(pagination.prev_page_url)">Previous</a>
                                        </li>

                                        <li class="page-item disabled"><a class="page-link text-dark" href="#">Page @{{ pagination.current_page }} of
                                                @{{ pagination.last_page }}</a></li>

                                        <li v-bind:class="[{disabled: !pagination.next_page_url}]" class="page-item"><a class="page-link" href="#"
                                                                                                                        @click="getItems(pagination.next_page_url)">Next</a>
                                        </li>
                                    </ul>
                                </nav>

                                <div v-if="loading"><i class="fa fa-spin fa-spinner"></i> </div>

                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>From</th>
                                        <th>To</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Payment Status</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="item in items">
                                        <td>@{{item.did}}</td>
                                        <td>@{{item.fname}} @{{item.sname}}</td>
                                        <td>@{{item.from}}</td>
                                        <td>@{{item.to}}</td>
                                        <td>@{{item.phone}}</td>
                                        <td>@{{item.email}}</td>
                                        <td>@{{item.paymentStatus}}</td>
                                        <td>@{{item.status}}</td>
                                        <td></td>
                                    </tr>

                                    </tbody>
                                </table>

                            </div>

                            <script>
                                Vue.use(axios);
                                let app = new Vue({
                                    el: '#app',
                                    data() {
                                        return {
                                            by: 'fname',
                                            term: '',
                                            length: 10,
                                            items: [],
                                            page_url: 'http://localhost:8000/api/',
                                            article_id: '',
                                            pagination: {},
                                            loading: false,
                                            edit: false
                                        };
                                    },
                                    created() {
                                        this.getItems();
                                    },
                                    methods: {
                                        getItems() {
                                            let self = this;
                                            self.loading = true;
                                            axios.post('api/test', {
                                                params: {
                                                    // ID: 12345
                                                }
                                            })
                                                .then(function (response) {

                                                    self.loading = true;
                                                    response = response.data;
                                                    self.items = response.data;
                                                    self.makePagination(response);
                                                    console.log(response.data);
                                                })
                                                .catch(function (error) {
                                                    console.log(error);
                                                });

                                        },
                                        makePagination(data) {
                                            let pagination = {
                                                current_page: data.current_page,
                                                last_page: data.last_page,
                                                next_page_url: data.next_page_url,
                                                prev_page_url: data.prev_page_url
                                            };
                                            this.pagination = pagination;
                                        },
                                        search() {

                                            let data = {
                                                by: this.by,
                                                term: this.term,
                                                length: this.length
                                            };

                                            let self = this;
                                            self.loading = true;

                                            axios.post('api/test', data)
                                                .then(function (response) {

                                                    self.loading = false;
                                                    response = response.data;
                                                    self.items = response.data;
                                                    self.makePagination(response);
                                                    console.log(response.data);
                                                })
                                                .catch(function (error) {
                                                    console.log(error);
                                                });


                                        }
                                    }
                                })
                            </script>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection