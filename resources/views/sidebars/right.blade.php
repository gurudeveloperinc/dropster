<!-- .right panel -->
<div class="right-side-panel">
    <div class="scrollable-right">

        <h3 class="title-heading">Tasks</h3>
        <div class="notes-widgets">
            <!-- .Notes header -->
            <div class="header-part">
                <div class="btn-group">
                    <button aria-expanded="false" data-toggle="dropdown" class="dropdown-toggle daydrop" type="button"> This week <span class="caret"></span></button>
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="#">Todays</a></li>
                        <li><a href="#">Monthly</a></li>
                        <li><a href="#">Yearly</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                </div>
                <div class="pull-right"> <a href="javascript:void(0)" class="text-inverse m-r-5" data-toggle="tooltip" title="Delete"><i class="ti-trash"></i></a> <a href="javascript:void(0)" data-toggle="tooltip" title="Add New" data-placement="left" class="text-inverse"><i class="ti-plus"></i></a> </div>
            </div>
            <!-- /.Notes header -->
            <!-- .Notes body -->
            <ul class="list-task list-group">
                <li class="list-group-item">
                    <div class="checkbox checkbox-success">
                        <input type="checkbox" id="inputSchedule1" name="inputCheckboxesSchedule">
                        <label for="inputSchedule1"> <span>Vehicle Inspection 2</span> </label>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="checkbox checkbox-success">
                        <input type="checkbox" id="inputCall1" name="inputCheckboxesCall">
                        <label for="inputCall1"> <span>Give Dropper report</span> </label>
                    </div>
                </li>
                <li class="list-group-item" data-role="task">
                    <div class="checkbox checkbox-success">
                        <input type="checkbox" id="inputBook1" name="inputCheckboxesBook">
                        <label for="inputBook1"> <span>Social Media</span> </label>
                    </div>
                </li>
                <li class="list-group-item" data-role="task">
                    <div class="checkbox checkbox-success">
                        <input type="checkbox" id="inputForward1" name="inputCheckboxesForward">
                        <label for="inputForward1"> <span>Forward all tasks</span> </label>
                    </div>
                </li>
                <li class="list-group-item" data-role="task">
                    <div class="checkbox checkbox-success">
                        <input type="checkbox" id="inputRecieve1" name="inputCheckboxesRecieve">
                        <label for="inputRecieve1"> <span>Support Tickets</span> </label>
                    </div>
                </li>
                <li class="list-group-item" data-role="task">
                    <div class="checkbox checkbox-success">
                        <input type="checkbox" id="inputForward21" name="inputCheckboxesd">
                        <label for="inputForward21"> <span>Staff Meeting</span> </label>
                    </div>
                </li>
            </ul>
            <!-- /.Notes body --><a class="text-muted db text-center m-t-10" href="javascript:void(0)">View all notes</a> </div>
        <!-- .Notification -->
        <h3 class="title-heading">Notifications <span class="label label-rouded label-danger pull-right">4</span></h3>
        <div class="message-center p-20">
            <a href="javascript:void(0)">
                <div class="mail-contnet">
                    <h5>Dropper Meeting</h5> <span class="mail-desc">Important Notification See you at</span> <span class="time">9:10 AM</span> </div>
            </a>
            <a href="javascript:void(0)">
                <div class="mail-contnet">
                    <h5>Register Vehicle</h5> <span class="mail-desc">Important Notification</span> <span class="time">9:08 AM</span> </div>
            </a>
            <a href="javascript:void(0)">
                <div class="mail-contnet">
                    <h5>Support Tickets</h5> <span class="mail-desc">Important Notification</span> <span class="time">9:08 AM</span> </div>
            </a>
            <a href="javascript:void(0)" class="b-none">
                <div class="mail-contnet">
                    <h5>Support Tickets</h5> <span class="mail-desc">Important Notification</span> <span class="time">9:02 AM</span> </div>
            </a>
        </div>
        <!-- /.Notification -->

    </div>
</div>
<!-- /.right panel -->

