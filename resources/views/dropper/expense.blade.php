@extends('layouts.app')

@section('content')

    <style>
        .form-group input,.form-group textarea{
            margin-bottom: 10px;
        }

    </style>

    <div class="container-fluid">
        <div id="page-wrapper">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-lg-12">
                        @include('notification')
                        <div class="white-box" style="height: 100%;">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="panel panel-default">

                                    <div class="panel-body">

                                        <form method="post" action="{{url('dropper-expense')}}">
                                            {{csrf_field()}}

                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Dropper</label>
                                                <div class="col-md-6">
                                                    <select class="selectpicker" data-live-search="true" name="drid">
                                                        @foreach($droppers as $dropper)
                                                            <option value="{{$dropper->drid}}">
                                                                {{$dropper->Customer->fname}} {{$dropper->Customer->sname}}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <br><br>

                                            <div class="form-group">
                                                <div class="col-md-4"></div>
                                                <div class="col-md-6" align="center">
                                                    <input id="fuelcheckbox" name="fuel" type="checkbox">
                                                    <label for="fuelcheckbox">Fuel</label>

                                                    <input id="airtimecheckbox" name="airtime" type="checkbox">
                                                    <label for="airtimecheckbox">Airtime</label>

                                                    <input id="datacheckbox" name="data" type="checkbox">
                                                    <label for="datacheckbox">Data</label>

                                                    <input id="maintenancecheckbox" name="maintenance" type="checkbox">
                                                    <label for="maintenancecheckbox">Maintenance</label>
                                                </div>

                                            </div>

                                            <div class="form-group airtime hidden">
                                                <label class="col-md-4 control-label">Airtime Amount</label>
                                                <div class="col-md-6">
                                                    <input type="number" class="form-control" value="{{old('airtimeAmount')}}" placeholder="Airtime Amount" name="airtimeAmount">
                                                </div>
                                            </div>

                                            <div class="form-group airtime hidden">
                                                <label class="col-md-4 control-label">Airtime Network</label>
                                                <div class="col-md-6">
                                                    <select class="form-control" name="airtimeNetwork">
                                                        <option>MTN</option>
                                                        <option>Glo</option>
                                                        <option>Airtel</option>
                                                        <option>9 Mobile</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group data hidden">
                                                <label class="col-md-4 control-label">Data Amount</label>

                                                <div class="col-md-6">
                                                    <input type="number" class="form-control" placeholder="Data Amount" name="dataAmount">
                                                </div>
                                            </div>

                                            <div class="form-group data hidden">
                                                <label class="col-md-4 control-label">Data Network</label>

                                                <div class="col-md-6">
                                                    <select class="form-control" name="dataNetwork">
                                                        <option>MTN</option>
                                                        <option>Glo</option>
                                                        <option>Airtel</option>
                                                        <option>9 Mobile</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group fuel hidden">
                                                <label class="col-md-4 control-label">Fuel Amount</label>

                                                <div class="col-md-6">
                                                    <input type="number" class="form-control" placeholder="Fuel Amount" name="fuelAmount">
                                                </div>
                                            </div>

                                            <div class="form-group fuel hidden">
                                                <label class="col-md-4 control-label">Fuel Station</label>

                                                <div class="col-md-6">
                                                    <input class="form-control" placeholder="Fuel Station" name="fuelStation">
                                                </div>
                                            </div>

                                            <div class="form-group maintenance hidden">
                                                <label class="col-md-4 control-label">Maintenance Amount</label>

                                                <div class="col-md-6">
                                                    <input type="number" class="form-control" placeholder="Maintenance Amount" name="maintenanceAmount">
                                                </div>
                                            </div>

                                            <div class="form-group maintenance hidden">
                                                <label class="col-md-4 control-label">Maintenance Brief</label>

                                                <div class="col-md-6">
                                                    <input class="form-control" placeholder="Brief Description" name="maintenanceBrief">
                                                </div>
                                            </div>

                                            <div class="form-group maintenance hidden">
                                                <label class="col-md-4 control-label">Maintenance Full Description (Optional)</label>

                                                <div class="col-md-6">
                                                    <textarea class="form-control" name="maintenanceFull"></textarea>
                                                </div>
                                            </div>


                                            <div class="form-group" >
                                                <div class="col-md-10 col-md-offset-4">

                                                    <a href="{{url('/')}}" style="margin-left:10px;" class="btn btn-warning pull-right">Back</a>
                                                    <button type="submit" class="btn btn-primary pull-right">
                                                        Submit
                                                    </button>
                                                </div>
                                            </div>


                                        </form>

                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        $(document).ready(function(){
            var fuelcheckbox = $('#fuelcheckbox');
            var airtimecheckbox = $('#airtimecheckbox');
            var datacheckbox = $('#datacheckbox');
            var maintenancecheckbox = $('#maintenancecheckbox');
            var fuelDiv = $('.fuel');
            var airtimeDiv = $('.airtime');
            var dataDiv = $('.data');
            var maintenanceDiv = $('.maintenance');

            function check(item,div) {
                if(item.is(":checked")) div.removeClass('hidden'); else div.addClass('hidden');
            }

            fuelcheckbox.on('change',function () {
                check(fuelcheckbox,fuelDiv);
            });
            airtimecheckbox.on('change',function () {
                check(airtimecheckbox,airtimeDiv);
            });
            datacheckbox.on('change',function () {
                check(datacheckbox,dataDiv);
            });
            maintenancecheckbox.on('change',function () {
                check(maintenancecheckbox,maintenanceDiv);
            });

        });
    </script>

@endsection


