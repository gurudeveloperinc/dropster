@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- .row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Registered Droppers</h3>
                            <table id="demo-foo-addrow" class="table table-bordered table-hover toggle-circle" data-page-size="50">
                                <thead>
                                <tr>
                                    <th data-sort-initial="true" data-toggle="true">First Name</th>
                                    <th>Last Name</th>
                                    <th>Phone</th>
                                    <th data-hide="phone, tablet">Email</th>
                                    <th data-hide="phone, tablet">Un-remitted</th>
                                    <th data-sort-ignore="true" class="min-width"> </th>
                                </tr>
                                </thead>
                                <div class="form-inline padding-bottom-15">
                                    <div class="row">

                                    </div>
                                    <div class="col-sm-12 text-right m-b-20 pull-right">
                                        <div class="form-group">
                                            <input id="demo-input-search2" type="text" placeholder="Search" class="form-control" autocomplete="off">
                                        </div>
                                    </div>
                                </div>

                                <tbody>

                                @foreach($droppers as $item)
                                    <tr>
                                        <td>{{$item->fname}}</td>
                                        <td>{{$item->sname}}</td>
                                        <td>{{$item->phone}}</td>
                                        <td>{{$item->email}}</td>
                                        <td>{{$item->Dropper->unremittedCash + $item->Dropper->unremittedPOS}}</td>
                                        <td>
                                            <a href="{{url('dropper/' . $item->cid)}}">
                                                <span class="label label-success">View Profile</span>
                                            </a>

                                            <a href="{{url('edit-customer/' . $item->cid)}}">
                                                <span class="label label-warning">Edit</span>
                                            </a>

                                            <a href="{{url('suspend-customer/' . $item->cid)}}">
                                                <span class="label label-danger">Suspend</span>
                                            </a>

                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>

                                <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <div class="text-right">
                                            <ul class="pagination">
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection