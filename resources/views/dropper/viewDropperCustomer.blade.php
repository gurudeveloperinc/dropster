@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Droppers</div>

                    <div class="panel-body">


                        <table class="table table-hover table-responsive ">
<div class="container" style="margin-bottom: 10px;">
                            <div class="col-md-8">
                            <form>
                                <input type="search" name="search" class="form-control" placeholder="searh..">
                            </form>
                            </div>


</div>
                            <th>Customer ID</th>
                            <th>First Name</th>
                            <th>SurName</th>
                            <th>Email</th>
                            <th>Gender</th>
                            <th>PhoneNo</th>
                            <th>Address</th>
                            <th>Balance</th>
                            <th>Role</th>


                            @foreach($customers as $customer)
                                <tr>

                                    <td>
                                        <a href="{{url('select-dropper/' . $customer->cid)}}" >
                                            @if(isset($customer->fname))
                                                {{$customer->fname}}
                                            @else
                                                {{$customer->fname}} {{$customer->sname}}
                                            @endif


                                        </a>

                                    </td>

                                    <td>{{$customer->fname}}</td>
                                    <td>{{$customer->sname}}</td>
                                    <td>{{$customer->email}}</td>
                                    <td>{{$customer->gender}}</td>
                                    <td>{{$customer->phone}}</td>
                                    <td>{{$customer->address}}</td>
                                    <td>{{$customer->balance}}</td>
                                    <td>{{$customer->role}}</td><br>


                                </tr>


                                <a href="{{url('view-customer/' . $customer->cid )}}" class="btn btn-primary">
                                    Assign Vehicle
                                </a>
                                <a href="{{url('add-vehicle-partner/' . $customer->cid )}}" class="btn btn-primary">
                                    Make Vehicle Partner
                                </a>
                            @endforeach

                        </table>




                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection