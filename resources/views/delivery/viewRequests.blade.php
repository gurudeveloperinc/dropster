<?php use Illuminate\Support\Facades\Input; ?>
@extends('layouts.app')

@section('content')


    <script src="{{url('js/vue.js')}}" ></script>
    <script src="{{url('js/axios.min.js')}}"></script>

    <div class="container-fluid" >
        <div id="page-wrapper">
            <div class="container-fluid">


                <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">


                            <div id="app" xmlns:v-bind="http://www.w3.org/1999/xhtml"
                                 xmlns:v-on="http://www.w3.org/1999/xhtml">

                                <h3 class="box-title m-b-0">DELIVERY REQUESTS
                                    <span v-if="status == 'Issue'" class="label label-success">Filtered by Issues</span>
                                    <span v-if="status == 'Scheduled'"  class="label label-success">Filtered by Scheduled</span>
                                    <span v-if="status == 'Complete'"  class="label label-success">Filtered by Completed</span>
                                    <span v-if="status == 'Available'"  class="label label-success">Filtered by Availability</span>
                                    <span v-if="status == 'Pending'"  class="label label-success">Filtered by Pending</span>
                                    <span v-if="status == 'Cancelled' "  class="label label-success">Filtered by Cancelled</span>
                                    <span v-if="status == 'unpaid' "  class="label label-success">Filtered by Unpaid</span>
                                </h3>

                                <br>

                                <a style="float:right;" href="{{url('switch-requests-page')}}" class="btn btn-primary">Switch to old layout</a>

                                <label class="box-title m-b-0">Search Requests By: </label>
                                <select v-model="by">
                                    <option selected value="fname">Firstname</option>
                                    <option value="sname">Surname</option>
                                    <option value="email">Email</option>
                                    <option value="phone">Phone</option>
                                    <option value="from">From</option>
                                    <option value="to">To</option>
                                    <option value="status">Status</option>
                                    <option value="paymentStatus">Payment Status</option>
                                </select>

                                <input type="text"  v-model="term" v-on:keyup="search">
                                <br>

                                <label class="box-title m-b-0">Filter Requests By: </label>
                                <select v-model="status" v-on:change="statusFilter">
                                    <option selected>Available</option>
                                    <option>Scheduled</option>
                                    <option>Complete</option>
                                    <option>Pending</option>
                                    <option>Issue</option>
                                    <option>Cancelled</option>
                                    <option value="unpaid">Un-Paid</option>
                                </select>
                                <br>


                                <nav aria-label="Page navigation example">
                                    <ul class="pagination">
                                        <li v-bind:class="[{disabled: !pagination.first_page_url}]" class="page-item"><a class="page-link" href="#"
                                                                                                                        @click="getItems(pagination.first_page_url)">First</a>
                                        </li>

                                        <li v-bind:class="[{disabled: !pagination.prev_page_url}]" class="page-item"><a class="page-link" href="#"
                                                                                                                        @click="getItems(pagination.prev_page_url)">Previous</a>
                                        </li>

                                        <li class="page-item disabled"><a class="page-link text-dark" href="#">Page @{{ pagination.current_page }} of
                                                @{{ pagination.last_page }}</a></li>

                                        <li v-bind:class="[{disabled: !pagination.next_page_url}]" class="page-item"><a class="page-link" href="#"
                                                                                                                        @click="getItems(pagination.next_page_url)">Next</a>
                                        </li>

                                        <li v-bind:class="[{disabled: !pagination.last_page_url}]" class="page-item"><a class="page-link" href="#"
                                                                                                                        @click="getItems(pagination.last_page_url)">Last</a>
                                        </li>
                                    </ul>
                                </nav>

                                <label class="box-title m-b-0">@{{ total }} Total Records</label>

                                <div style="float: right;">
                                    <span style="font-size: 24px;color: darkgreen;">&#x20A6;@{{totalAmount}}</span> &nbsp;<br>
                                    <label>Table Length: </label>
                                    <select v-model="length" v-on:change="search">
                                        <option selected>10</option>
                                        <option>20</option>
                                        <option>50</option>
                                        <option>100</option>
                                        <option>200</option>
                                    </select>

                                </div>

                                <div v-if="loading"><i class="fa fa-spin fa-spinner"></i> </div>

                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>DID</th>
                                        <th>Requested By</th>
                                        <th>To Be Received By</th>
                                        <th>Dropper</th>
                                        <th>Amount</th>
                                        <th>Payment</th>
                                        <th>Status</th>
                                        <th>Created</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="item in items">
                                        <td>@{{item.did}}
                                            <i v-if="item.droppedOffAt" class="fa fa-dropbox" style="color: green;"></i>
                                        </td>
                                        <td>
                                            <a v-bind:href="item.customer_url">
                                                @{{item.customer.fname}} @{{item.customer.sname}}
                                            </a>
                                        </td>
                                        <td>@{{item.fname}} @{{ item.sname }}</td>
                                        <td>
                                            <a v-bind:href="item.dropper_url" v-if="item.dropper">
                                                @{{item.dropper.customer.fname}} @{{item.dropper.customer.sname}}
                                            </a>

                                            <span v-if="!item.dropper">No Dropper yet</span>
                                        </td>
                                        <td>@{{item.amount}}</td>
                                        <td>
                                            <label v-if="item.paymentStatus == 'paid'" class="label label-success">Paid with @{{item.paymentMethod}}</label>
                                            <label v-if="item.paymentStatus == 'unpaid'" class="label label-danger">Un-Paid</label>
                                        </td>
                                        <td>
                                            <label v-if="item.status == 'Pending'" class="label label-warning">Pending</label>
                                            <label v-if="item.status == 'Complete'" class="label label-success">Complete</label>
                                            <label v-if="item.status == 'Available'" class="label label-primary">Available</label>
                                            <label v-if="item.status == 'Issue'"  class="label label-danger">Issue</label>
                                            <label v-if="item.status == 'Cancelled'" class="label label-default" style="background-color: saddlebrown">Cancelled</label>
                                        </td>
                                        <td>@{{ item.created_at }}</td>
                                        <td>
                                            <a v-bind:href="item.url">
                                                <span class="label label-success">View </span>
                                            </a>

                                        </td>
                                    </tr>

                                    </tbody>
                                </table>

                            </div>

                            <script>
                                Vue.use(axios);
                                let app = new Vue({
                                    el: '#app',
                                    data() {
                                        return {
                                            by: 'fname',
                                            term: '',
                                            length: 10,
                                            items: [],
                                            page_url: '{{url('/requests-table')}}',
                                            pagination: {},
                                            loading: false,
                                            edit: false,
                                            totalAmount : 0,
                                            last_page_url: "requests-table?page=",
                                            first_page_url: "requests-table?page=",
                                            status:'{{Input::get('by')}}'
                                        };
                                    },
                                    created() {
                                        this.statusFilter();
                                    },
                                    methods: {
                                        getItems(url) {
                                            let self = this;
                                            self.loading = true;
                                            let data = {
                                                _token: "{{csrf_token()}}",
                                                length: this.length,
                                            };
                                            axios.post(url, data)
                                                .then(function (response) {

                                                    self.loading = false;
                                                    response = response.data;
                                                    self.totalAmount = 0;
                                                    for(let i=0; i < response.data.length; i++) {

                                                        response.data[i].url = "view-delivery-detail/" + response.data[i].did;
                                                        response.data[i].customer_url = "view-customer/" + response.data[i].cid;
                                                        response.data[i].dropper_url = "dropper/" + response.data[i].drid;
                                                        self.totalAmount += Number(response.data[i].amount);
                                                    }

                                                    self.items = response.data;
                                                    self.makePagination(response);
                                                    console.log(response);
                                                })
                                                .catch(function (error) {
                                                    self.loading = false;
                                                    console.log(error);
                                                });

                                        },
                                        makePagination(data) {
                                            let pagination = {
                                                last_page_url: this.last_page_url + data.last_page,
                                                first_page_url: this.first_page_url + data.first_page,
                                                current_page: data.current_page,
                                                last_page: data.last_page,
                                                next_page_url: data.next_page_url,
                                                prev_page_url: data.prev_page_url
                                            };
                                            this.total = data.total;
                                            this.pagination = pagination;
                                        },
                                        search() {

                                            let data = {
                                                by: this.by,
                                                term: this.term,
                                                length: this.length,
                                                _token: "{{csrf_token()}}"
                                            };

                                            let self = this;
                                            self.loading = true;

                                            axios.post(self.page_url, data)
                                                .then(function (response) {

                                                    self.loading = false;
                                                    response = response.data;
                                                    console.log(response);
                                                    self.totalAmount = 0;
                                                    for(let i=0; i < response.data.length; i++) {

                                                        response.data[i].url = "view-delivery-detail/" + response.data[i].did;
                                                        response.data[i].customer_url = "view-customer/" + response.data[i].cid;
                                                        response.data[i].dropper_url = "dropper/" + response.data[i].drid;
                                                        self.totalAmount += Number(response.data[i].amount);

                                                    }

                                                    self.items = response.data;
                                                    self.makePagination(response);

                                                })
                                                .catch(function (error) {
                                                    self.loading = false;
                                                    console.log(error);
                                                });

                                        },
                                        statusFilter(){

                                            if(this.status === "unpaid") {
                                                this.unpaid();
                                            } else {
                                                this.by = "status";
                                                this.term = this.status;
                                                this.search();

                                            }

                                        },
                                        unpaid() {

                                            let data = {
                                                length: this.length,
                                                _token: "{{csrf_token()}}"

                                            };

                                            let self = this;
                                            self.loading = true;

                                            axios.post('unpaid-requests-table', data)
                                                .then(function (response) {

                                                    self.loading = false;
                                                    response = response.data;
                                                    self.totalAmount = 0;
                                                    for(let i=0; i < response.data.length; i++) {

                                                        response.data[i].url = "view-delivery-detail/" + response.data[i].did;
                                                        response.data[i].customer_url = "view-customer/" + response.data[i].cid;
                                                        response.data[i].dropper_url = "dropper/" + response.data[i].drid;
                                                        self.totalAmount += Number(response.data[i].amount);

                                                    }

                                                    self.items = response.data;
                                                    self.makePagination(response);
                                                    console.log(response.data);
                                                })
                                                .catch(function (error) {
                                                    self.loading = false;
                                                    console.log(error);
                                                });

                                        },


                                    }
                                })
                            </script>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection